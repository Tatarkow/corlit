# Corlit
---
Corlit is a cross-platform puzzle game. Try to color the cube according to the task by rolling it on a board, while the bottom face of the cube will always take color of the board plane under it.

## Trailer
Announcement trailer can be watched [here](https://www.youtube.com/watch?v=pL3uvb3-V_c).

## How to install it?
Corlit needs to be run as Python (2) script. Please, download zipped repository [here](https://bitbucket.org/Tatarkow/corlit/downloads/), unzip it and run in with `python main.py`. Note that you need to have all libraries installed. That being said, you will also need to have installed following:

1. Python
1. Kivy