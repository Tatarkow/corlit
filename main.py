#! /usrs/bitn/env python

from kivy.utils import platform
from kivy.app import App
from kivy.animation import Animation
from kivy.uix.widget import Widget
from kivy.uix.popup import Popup
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.textinput import TextInput
from kivy.uix.button import Button
from kivy.core.window import Window
from kivy.config import Config
from kivy.graphics import Color, Rectangle
from kivy.properties import *
from kivy.clock import Clock
from kivy.resources import resource_find
from kivy.graphics.transformation import Matrix
from kivy.logger import Logger
from kivy.graphics import *
from kivy.graphics.opengl import *
from kivy.core.audio import SoundLoader
from kivy.core.clipboard import Clipboard
from kivy.network.urlrequest import UrlRequest
from kivy.storage.jsonstore import JsonStore

import random, math
from functools import partial
from os.path import join, exists
from os import makedirs

# Please do not read this code. It is old project of mine and I am ashamed how
# badly it is writen.

class Cube(Widget):
	view_mode = StringProperty("Goal")
	view_mode_single = BooleanProperty(True)
	view_mode_string = StringProperty("")

	def __init__(self):
		self.pos1 = Translate(0, -1.3, -7) # global
		self.pos2 = Translate(0, 0, 0) # on board
		self.rotx = Rotate(90, 1, 0, 0)
		self.roty = Rotate(90, 0, 1, 0)
		self.rotz = Rotate(90, 0, 0, 1)
		self.rotx_dev2 = 0
		self.roty_dev2 = 0
		self.rotz_dev2 = 0
		self.pos3 = Translate(0, 0, 0) # for rolling

		self.colors = {}
		self.goal_colors = {}
		self.sides = {"front": 0, "back": 1, "top": 2, "bottom": 3, "left": 4, "right": 5}
		self.face_vertices = {}
		self.inside_vertices = {}

		a = 1.0
		b = a*5/6

		self.face_vertices["front"] = [[-a,-a, a,    a,-a, a,    b,-b, a,   -b,-b, a],
										[ b,-b, a,    a,-a, a,    a, a, a,    b, b, a],
										[-b, b, a,    b, b, a,    a, a, a,   -a, a, a],
										[-a,-a, a,   -b,-b, a,   -b, b, a,   -a, a, a]]

		self.face_vertices["back"] = [[-a,-a,-a,    a,-a,-a,    b,-b,-a,   -b,-b,-a], # back
									   [ b,-b,-a,    a,-a,-a,    a, a,-a,    b, b,-a],
									   [-b, b,-a,    b, b,-a,    a, a,-a,   -a, a,-a],
									   [-a,-a,-a,   -b,-b,-a,   -b, b,-a,   -a, a,-a]]

		self.face_vertices["top"] = [[-a, a, a,    a, a, a,    b, a, b,   -b, a, b], # top
									  [ b, a, b,    a, a, a,    a, a,-a,    b, a,-b],
									  [-a, a,-a,    a, a,-a,    b, a,-b,   -b, a,-b],
									  [-b, a, b,   -a, a, a,   -a, a,-a,   -b, a,-b]]

		self.face_vertices["bottom"] = [[-a,-a, a,    a,-a, a,    b,-a, b,   -b,-a, b], # bottom
										 [ b,-a, b,    a,-a, a,    a,-a,-a,    b,-a,-b],
										 [-a,-a,-a,    a,-a,-a,    b,-a,-b,   -b,-a,-b],
										 [-b,-a, b,   -a,-a, a,   -a,-a,-a,   -b,-a,-b]]

		self.face_vertices["left"] = [[-a,-a, a,   -a,-a,-a,   -a,-b,-b,   -a,-b, b], # left
									   [-a,-b,-b,   -a,-a,-a,   -a, a,-a,   -a, b,-b],
									   [-a, a, a,   -a, a,-a,   -a, b,-b,   -a, b, b],
									   [-a,-b, b,   -a,-a, a,   -a, a, a,   -a, b, b]]

		self.face_vertices["right"] = [[ a,-a, a,    a,-a,-a,    a,-b,-b,    a,-b, b], # right
										[ a,-b,-b,    a,-a,-a,    a, a,-a,    a, b,-b],
										[ a, a, a,    a, a,-a,    a, b,-b,    a, b, b],
										[ a,-b, b,    a,-a, a,    a, a, a,    a, b, b]]


		self.inside_vertices["front"] = [[-b,-b, a,    b,-b, a,    b,-b, b,   -b,-b, b],
										  [ b,-b, a,    b, b, a,    b, b, b,    b,-b, b],
										  [-b, b, a,    b, b, a,    b, b, b,   -b, b, b],
										  [-b,-b, a,   -b, b, a,   -b, b, b,   -b,-b, b]]

		self.inside_vertices["back"] = [[-b,-b,-a,    b,-b,-a,    b,-b,-b,   -b,-b,-b],
										 [ b,-b,-a,    b, b,-a,    b, b,-b,    b,-b,-b],
										 [-b, b,-a,    b, b,-a,    b, b,-b,   -b, b,-b],
										 [-b,-b,-a,   -b, b,-a,   -b, b,-b,   -b,-b,-b]]

		self.inside_vertices["top"] = [[-b, b, b,    b, b, b,    b, a, b,   -b, a, b],
										[ b, b, b,    b, a, b,    b, a,-b,    b, b,-b],
										[-b, b,-b,    b, b,-b,    b, a,-b,   -b, a,-b],
										[-b, b, b,   -b, a, b,   -b, a,-b,   -b, b,-b]]

		self.inside_vertices["bottom"] = [[-b,-b, b,    b,-b, b,    b,-a, b,   -b,-a, b],
										   [ b,-b, b,    b,-a, b,    b,-a,-b,    b,-b,-b],
										   [-b,-b,-b,    b,-b,-b,    b,-a,-b,   -b,-a,-b],
										   [-b,-b, b,   -b,-a, b,   -b,-a,-b,   -b,-b,-b]]

		self.inside_vertices["left"] = [[-a,-b,-b,   -b,-b,-b,   -b,-b, b,   -a,-b, b],
										 [-a,-b,-b,   -b,-b,-b,   -b, b,-b,   -a, b,-b],
										 [-a, b,-b,   -b, b,-b,   -b, b, b,   -a, b, b],
										 [-a,-b, b,   -b,-b, b,   -b, b, b,   -a, b, b]]

		self.inside_vertices["right"] = [[ a,-b,-b,    b,-b,-b,    b,-b, b,    a,-b, b],
										  [ a,-b,-b,    b,-b,-b,    b, b,-b,    a, b,-b],
										  [ a, b,-b,    b, b,-b,    b, b, b,    a, b, b],
										  [ a,-b, b,    b,-b, b,    b, b, b,    a, b, b]]

	def create(self):
		self.pos1 = Translate(self.pos1.x, self.pos1.y, self.pos1.z) # global
		self.rot2 = Rotate(30, 1, 0, 0)
		self.pos2 = Translate(self.pos2.x, self.pos2.y, self.pos2.z) # on board
		self.rotx = Rotate(self.rotx.angle, 1, 0, 0)
		self.roty = Rotate(self.roty.angle, 0, 1, 0)
		self.rotz = Rotate(self.rotz.angle, 0, 0, 1)
		self.pos3 = Translate(self.pos3.x, self.pos3.y, self.pos3.z) # for rolling
		
		for i in self.sides:
					self.render(self.face_vertices[i], self.colors[i])
					self.render(self.inside_vertices[i], [self.colors[i][0] - 0.2, self.colors[i][1] - 0.2, self.colors[i][2] - 0.2])

		self.rotx_dev = Rotate(self.rotx_dev2, 1, 0, 0)
		self.roty_dev = Rotate(self.roty_dev2, 0, 1, 0)
		self.rotz_dev = Rotate(self.rotz_dev2, 0, 0, 1)
		
		a = 1.0
		b = a*11/12
		c = a/6

		if self.goal_colors["front"] != (0.9333333333333333, 0.9333333333333333, 0.9333333333333333):
			self.front_dev =                [[-c, c,a,   c, c,a,   c,-c,a,   -c,-c,a]]
			self.front_inner_dev =          [[-c, c,a,   c, c,a,   c, c,b,   -c, c,b],
											[ -c, c,a,  -c, c,b,  -c,-c,b,   -c,-c,a],
											[  c, c,a,   c, c,b,   c,-c,b,    c,-c,a],
											[ -c,-c,a,   c,-c,a,   c,-c,b,   -c,-c,b]]
			self.front_super_inner_dev =    [[-c, c,b,   c, c,b,   c,-c,b,   -c,-c,b]]
			self.render(self.front_dev, self.goal_colors["front"])
			self.render(self.front_inner_dev, [self.goal_colors["front"][0]-0.1, self.goal_colors["front"][1]-0.1, self.goal_colors["front"][2]-0.1])
			self.render(self.front_super_inner_dev, [self.goal_colors["front"][0]-0.2, self.goal_colors["front"][1]-0.2, self.goal_colors["front"][2]-0.2])
		
		if self.goal_colors["back"] != (0.9333333333333333, 0.9333333333333333, 0.9333333333333333):
			self.back_dev =                 [[-c, c,-a,   c, c,-a,   c,-c,-a,   -c,-c,-a]]
			self.back_inner_dev =           [[-c, c,-a,   c, c,-a,   c, c,-b,   -c, c,-b],
											[ -c, c,-a,  -c, c,-b,  -c,-c,-b,   -c,-c,-a],
											[  c, c,-a,   c, c,-b,   c,-c,-b,    c,-c,-a],
											[ -c,-c,-a,   c,-c,-a,   c,-c,-b,   -c,-c,-b]]
			self.back_super_inner_dev =     [[-c, c,-b,   c, c,-b,   c,-c,-b,   -c,-c,-b]]
			self.render(self.back_dev, self.goal_colors["back"])
			self.render(self.back_inner_dev, [self.goal_colors["back"][0]-0.1, self.goal_colors["back"][1]-0.1, self.goal_colors["back"][2]-0.1])
			self.render(self.back_super_inner_dev, [self.goal_colors["back"][0]-0.2, self.goal_colors["back"][1]-0.2, self.goal_colors["back"][2]-0.2])

		if self.goal_colors["top"] != (0.9333333333333333, 0.9333333333333333, 0.9333333333333333):
			self.top_dev =                  [[-c, a,c,   c, a,c,   c, a,-c,  -c, a,-c]]
			self.top_inner_dev =            [[-c, a,c,   c, a,c,   c, b,c,   -c, b,c],
											[  c, a,c,   c, a,-c,  c, b,-c,   c, b,c],
											[  c, a,-c, -c, a,-c, -c, b,-c,   c, b,-c],
											[ -c, a,-c, -c, a,c,  -c, b,c,   -c, b,-c]]
			self.top_super_inner_dev =      [[-c, b,c,   c, b,c,   c, b,-c,  -c, b,-c]]
			self.render(self.top_dev, self.goal_colors["top"])
			self.render(self.top_inner_dev, [self.goal_colors["top"][0]-0.1, self.goal_colors["top"][1]-0.1, self.goal_colors["top"][2]-0.1])
			self.render(self.top_super_inner_dev, [self.goal_colors["top"][0]-0.2, self.goal_colors["top"][1]-0.2, self.goal_colors["top"][2]-0.2])

		if self.goal_colors["bottom"] != (0.9333333333333333, 0.9333333333333333, 0.9333333333333333):
			self.bottom_dev =               [[-c,-a,c,   c,-a,c,   c,-a,-c,  -c,-a,-c]]
			self.bottom_inner_dev =         [[-c,-a,c,   c,-a,c,   c,-b,c,   -c,-b,c],
											[  c,-a,c,   c,-a,-c,  c,-b,-c,   c,-b,c],
											[  c,-a,-c, -c,-a,-c, -c,-b,-c,   c,-b,-c],
											[ -c,-a,-c, -c,-a,c,  -c,-b,c,   -c,-b,-c]]
			self.bottom_super_inner_dev =   [[-c,-b,c,   c,-b,c,   c,-b,-c,  -c,-b,-c]]
			self.render(self.bottom_dev, self.goal_colors["bottom"])
			self.render(self.bottom_inner_dev, [self.goal_colors["bottom"][0]-0.1, self.goal_colors["bottom"][1]-0.1, self.goal_colors["bottom"][2]-0.1])
			self.render(self.bottom_super_inner_dev, [self.goal_colors["bottom"][0]-0.2, self.goal_colors["bottom"][1]-0.2, self.goal_colors["bottom"][2]-0.2])

		if self.goal_colors["left"] != (0.9333333333333333, 0.9333333333333333, 0.9333333333333333):
			self.left_dev =                 [[-a, c,c,  -a, c,-c, -a,-c,-c,  -a,-c,c]]
			self.left_inner_dev =           [[-a, c,c,  -a, c,-c, -b, c,-c,  -b, c,c],
											[ -a, c,-c, -a,-c,-c, -b,-c,-c,  -b, c,-c],
											[ -a,-c,-c, -a,-c,c,  -b,-c,c,   -b,-c,-c],
											[ -a,-c,c,  -a, c,c,  -b, c,c,   -b,-c,c]]
			self.left_super_inner_dev =     [[-b, c,c,  -b, c,-c, -b,-c,-c,  -b,-c,c]]
			self.render(self.left_dev, self.goal_colors["left"])
			self.render(self.left_inner_dev, [self.goal_colors["left"][0]-0.1, self.goal_colors["left"][1]-0.1, self.goal_colors["left"][2]-0.1])
			self.render(self.left_super_inner_dev, [self.goal_colors["left"][0]-0.2, self.goal_colors["left"][1]-0.2, self.goal_colors["left"][2]-0.2])
		
		if self.goal_colors["right"] != (0.9333333333333333, 0.9333333333333333, 0.9333333333333333):
			self.right_dev =                [[ a, c,c,   a, c,-c,  a,-c,-c,   a,-c,c]]
			self.right_inner_dev =          [[ a, c,c,   a, c,-c,  b, c,-c,   b, c,c],
											[  a, c,-c,  a,-c,-c,  b,-c,-c,   b, c,-c],
											[  a,-c,-c,  a,-c,c,   b,-c,c,    b,-c,-c],
											[  a,-c,c,   a, c,c,   b, c,c,    b,-c,c]]
			self.right_super_inner_dev =    [[ b, c,c,   b, c,-c,  b,-c,-c,   b,-c,c]]
			self.render(self.right_dev, self.goal_colors["right"])
			self.render(self.right_inner_dev, [self.goal_colors["right"][0]-0.1, self.goal_colors["right"][1]-0.1, self.goal_colors["right"][2]-0.1])
			self.render(self.right_super_inner_dev, [self.goal_colors["right"][0]-0.2, self.goal_colors["right"][1]-0.2, self.goal_colors["right"][2]-0.2])
	
		



	def render(self, vertices, color):
		for i in xrange(len(vertices)):
			ChangeState(Kd=color, Ka=color, Ks=(.3, .3, .3), Tr=1., Ns=1., intensity=1.)
			Mesh(vertices=vertices[i], indices=[0, 1, 2, 3, 0, 2], fmt=[('v_pos', 3, 'float')], mode='triangles')

class Game(Widget):
	cube = Cube()

	def __init__(self, **kw):
		super(Game, self).__init__(**kw)

		self.board = Board()
		
		self.cube.pos2.x = 0
		self.cube.pos2.z = 0
		



		"""
		kw['shader_file'] = 'shaders.glsl'
		self.canvas = RenderContext(compute_normal_mat=True)
		shader_file = kw.pop('shader_file')
		self.canvas.shader.source = resource_find(shader_file)
		with self.canvas:
			self.cb = Callback(self.setup_gl_context)
			self.setup_scene(0, 0, 0)
			self.cb = Callback(self.reset_gl_context)

		

		asp = float(Window.width) / Window.height / 2.0
		proj = Matrix().view_clip(-asp, asp, -0.5, 0.5, 1, 100, 1)
		self.canvas['projection_mat'] = proj

	def reset_canvas(self):
		self.canvas.clear()
		with self.canvas:
			self.cb = Callback(self.setup_gl_context)
			PushMatrix()
			self.setup_scene(0, 0, 0)
			PopMatrix()
			self.cb = Callback(self.reset_gl_context)
		""" 

	
	def reset_canvas(self, **kw):
		self.canvas.clear()
		with self.canvas:
			self.fbo = Fbo(with_depthbuffer = True, size = Window.size, clear_color = (0.4, 0.5, 0.6, 1))
			Rectangle(size=Window.size, texture=self.fbo.texture)

		kw['shader_file'] = 'shaders.glsl'
		shader_file = kw.pop('shader_file')   
		self.fbo.shader.source = resource_find(shader_file)

		with self.fbo:
			ClearColor(0.35, 0.5, 0.65, 1)
			ClearBuffers(clear_depth=True)
			self.cb = Callback(self.setup_gl_context)
			self.setup_scene(0, 0, 0)
			self.cb = Callback(self.reset_gl_context)

		asp = float(Window.width) / Window.height / 2.0
		proj = Matrix().view_clip(-asp, asp, -0.5, 0.5, 1, 100, 1)
		self.fbo['projection_mat'] = proj
	
	def setup_gl_context(self, *args):
		glEnable(GL_DEPTH_TEST)

	def reset_gl_context(self, *args):
		glDisable(GL_DEPTH_TEST)
	
	def setup_scene(self, dis_x, dis_z, dis_y):
		PushMatrix()
		self.board.create()
		PopMatrix()
		
		PushMatrix()
		self.cube.create()
		PopMatrix()	

class Board(Widget):
	def __init__(self):
		self.depth = 2
		self.pos1 = Translate(0, 0, 2) # global

		a = 1.0
		b = a*15/16
		c = 2*a - b

		self.plane_vertices = [[-b,-a, b,    b,-a, b,    b,-a,-b,   -b,-a,-b]]
		self.border_vertices = [[-a,-a, a,    a,-a, a,    b,-a, b,   -b,-a, b],
							  [ b,-a, b,    a,-a, a,    a,-a,-a,    b,-a,-b],
							  [-b,-a,-b,    b,-a,-b,    a,-a,-a,   -a,-a,-a],
							  [-a,-a, a,   -b,-a, b,   -b,-a,-b,   -a,-a,-a]]

	def create(self):
		self.pos1 = Translate(self.pos1.x, self.pos1.y, self.pos1.z) # global
		self.rot1 = Rotate(30, 1, 0, 0)
		
		for i in xrange(self.depth):
			for j in xrange(3):
				PushMatrix()
				Translate(j*2 - 2, 0, -i*2) # moves each tile somewhere else
				self.render(self.plane_vertices, self.colors[i*3 + j])
				self.render(self.border_vertices, [.93, .93, .93])
				PopMatrix()

	def render(self, vertices, color):
		for i in xrange(len(vertices)):
			ChangeState(Kd=color, Ka=color, Ks=(.3, .3, .3), Tr=1., Ns=1., intensity=1.)
			Mesh(vertices=vertices[i], indices=[0, 1, 2, 3, 0, 2], fmt=[('v_pos', 3, 'float')], mode='triangles')

class Root(FloatLayout):
	bg_color = (0.4, 0.5, 0.6)
	stats_from_level = NumericProperty(1)
	animations = BooleanProperty(True)

	level_no = NumericProperty(1)
	level_no_ending = StringProperty("st")
	moves_no = NumericProperty(0)
	moves_no_title = StringProperty("\nmoves")
	time = NumericProperty(0)
	time_string = StringProperty("00:00")
	victory_y = NumericProperty(-Window.height*0.6)
	moves_best = NumericProperty(0)
	moves_possible = NumericProperty(0)
	levels_unlocked = NumericProperty(0)
	resume_unsolved = BooleanProperty(False)
	resume_untried = BooleanProperty(False)
	resume_imperfect = BooleanProperty(False)
	stars = ListProperty(["!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!", "!"])
	unlock2_label = StringProperty("")
	unlock3_label = StringProperty("")
	unlock4_label = StringProperty("")
	unlock5_label = StringProperty("")
	unlock6_label = StringProperty("")

	def __init__(self, *larg, **kw):
		super(Root, self).__init__(*larg, **kw)
		
		# n = int(open("options.txt", "r").read()) + 1
		# print n
		# open("options.txt", "w").write(str(n))
		# self.test = int(open("options.txt", "r").read())
		self.ids.stats_label.bind(minimum_height=self.ids.stats_label.setter('height'))
		Window.bind(on_keyboard=self.back_click)
		
		self.history = ["self.menu_action(back=True)"]
		self.soundtrack = SoundLoader.load("soundtrack-Electroids.mp3")
		self.sound = SoundLoader.load("click.mp3")
		self.access_storage()
		self.read_settings()
		if self.ids.music.active == True:
			self.soundtrack.play()
			self.soundtrack.loop = True
		self.time2 = 0
		self.time3 = 0
		self.load_level(1)
		self.ids.game.cube.pos2.x = 0
		self.ids.game.cube.pos2.z = 0
		self.menu_rotate = True
		self.touch_down = [0, 0]
		self.active = False
		self.move_locked = True
		self.touch_reay = False
		self.rolling = False
		self.ids.game.reset_canvas()
		self.count_stars()
		self.menu_rotate_user = True
		self.rotation_speed_y = 0
		self.rotation_speed_x = 0

	def access_storage(self):
		self.data_dir = App().user_data_dir + "corlit/"
		print self.data_dir
		if not exists(self.data_dir):
			makedirs(self.data_dir)
		self.store_stats = JsonStore(join(self.data_dir, "stats.json"))
		self.store_settings = JsonStore(join(self.data_dir, "settings.json"))
		self.store_levels = JsonStore(join("./", "levels.json"))
		self.possible_moves = []
		for i in xrange(60):
			self.possible_moves.append(self.store_levels.get("level_" + str(i+1))["possible_moves"])
		print self.possible_moves
		
		if not self.store_settings.exists("music"):
			self.store_settings.put("music", volume=True)
		if not self.store_settings.exists("sounds"):
			self.store_settings.put("sounds", volume=True)
		if not self.store_settings.exists("resume"):
			self.store_settings.put("resume", action="unsolved")
		if not self.store_settings.exists("UI"):
			self.store_settings.put("UI", animations="True")

		if not self.store_stats.exists("level_1"):
			for i in xrange(60):
				self.store_stats.put("level_" + str(i+1),
									tries=0,
									finished=0,
									total_moves=0,
									finished_moves=0,
									least_moves=0,
									most_moves=0,
									average_moves=0,
									total_time=0,
									finished_time=0,
									shortest_time=0,
									longest_time=0,
									average_time=0)

	def read_settings(self):
		if self.store_settings.get("music")["volume"] == True:
			self.ids.music.active = True
			self.soundtrack.play()
			self.soundtrack.loop = True
		else:
			self.ids.music.active = False
			self.soundtrack.stop()
		if self.store_settings.get("sounds")["volume"] == True:
			self.ids.sound_volume.active = True
		else:
			self.ids.sound_volume.active = False
		if self.store_settings.get("resume")["action"] == "unsolved":
			self.resume_unsolved = True
		elif self.store_settings.get("resume")["action"] == "untried":
			self.resume_untried = True
		elif self.store_settings.get("resume")["action"] == "imperfect":
			self.resume_imperfect = True
		if self.store_settings.get("UI")["animations"] == "True":
			self.animations = True
		elif self.store_settings.get("UI")["animations"] == "False":
			self.animations = False

	def back_click(self, window, key, *largs):
		if key == 27:
			if len(self.history) > 1:
				eval(self.history[-2])
				self.history.pop()
				return True
			Window.close()

	def update(self, dt):
		if self.active == True:
			self.time += dt
		if int(self.time)%60 < 10:
			self.time_string = str(int(self.time)/60) + ":0" + str(int(self.time)%60)
		else:
			self.time_string = str(int(self.time)/60) + ":" + str(int(self.time)%60)
		if self.menu_rotate == True:
			if self.rotation_speed_y == 0 and self.rotation_speed_x == 0:
				self.ids.game.cube.rotx.angle += 0.05
				self.ids.game.cube.roty.angle += 0.1
				self.ids.game.cube.rotz.angle += 0.05
			else:
				self.ids.game.cube.rotx.angle += self.rotation_speed_x
				self.ids.game.cube.roty.angle += self.rotation_speed_y
			if self.rotation_speed_y > 0:
				self.rotation_speed_y -= 0.02
				if self.rotation_speed_y < 0:
					self.rotation_speed_y = 0
			elif self.rotation_speed_y < 0:
				self.rotation_speed_y += 0.02
				if self.rotation_speed_y > 0:
					self.rotation_speed_y = 0
			if self.rotation_speed_x > 0:
				self.rotation_speed_x -= 0.02
				if self.rotation_speed_x < 0:
					self.rotation_speed_x = 0
			elif self.rotation_speed_x < 0:
				self.rotation_speed_x += 0.02
				if self.rotation_speed_x > 0:
					self.rotation_speed_x = 0
		self.time3 += dt # for music
		self.time2 += dt # from app start

	def resume_action(self, level_no=0, back=False, *args):
		temp_resume_imperfect = False # protects opening unlocked levels
		if level_no == 0:
			if self.ids.resume_unsolved.active == True:
				for i in xrange(60):
					if self.stars[i] == "":
						if i+1 <= self.levels_unlocked:
							level_no = i+1
						else:
							temp_resume_imperfect = True
						break
			if self.ids.resume_untried.active == True:
				level_stats = [i.strip().split() for i in open("stats.txt").readlines()]
				for i in xrange(60):
					if int(level_stats[i+1][1]) == 0:
						if i+1 <= self.levels_unlocked:
							level_no = i+1
						else:
							temp_resume_imperfect = True
						break
			if self.ids.resume_imperfect.active == True or temp_resume_imperfect == True:
				for i in xrange(60):
					if self.stars[i] != "***":
						level_no = i+1
						break
		self.level_no = level_no
		self.menu_hide()
		self.levels_hide()
		self.game_show(level_no)
		if back != True:
			self.history.append("self.resume_action(level_no=" + str(level_no) + ", back=True)")

	def levels_action(self, back=False):
		self.game_hide()
		self.menu_hide()
		self.levels_show()

		if back != True:
			self.history.append("self.levels_action(back=True)")

	def settings_action(self, back=False):	
		self.menu_hide()
		self.settings_show()
		if back != True:
			self.history.append("self.settings_action(back=True)")

	def stats_action(self, back=False):		
		self.menu_hide()
		self.stats_show()
		if back != True:
			self.history.append("self.stats_action(back=True)")

	def credits_action(self, back=False):
		self.menu_hide()
		self.credits_show()
		if back != True:
			self.history.append("self.credits_action(back=True)")

	def quit_action(self):
		Window.close()

	def menu_action(self, back=False):
		if self.moves_no > 0 and self.move_locked == False:
			self.write_stats(False)
		if self.active == False:
			self.victory_hide()
		
		self.levels_hide()
		self.settings_hide()
		self.stats_hide()
		self.credits_hide()
		self.game_hide()
		self.menu_show()
		if back != True:
			self.history.append("self.menu_action(back=True)")

	def reset_action(self):
		if self.moves_no > 0 and self.move_locked == False:
			self.write_stats(False)
		self.load_level(self.level_no)
		if self.active == False:
			self.victory_hide()
			print "TADY"
			Animation(z = -5.0, t='out_cubic').start(self.ids.game.board.pos1)
		if self.ids.animations.active == True:
			Animation(angle = 390, t='out_cubic').start(self.ids.game.board.rot1)
			animation = Animation(angle = 390, t='out_cubic')
			animation.start(self.ids.game.cube.rot2)
			animation.bind(on_complete = self.next_action_finished)
		else:
			self.next_action_finished()
		self.moves_no = 0

	def prev_action(self):
		if self.level_no > 1:
			if self.moves_no > 0 and self.move_locked == False:
				self.write_stats(False)
			self.load_level(self.level_no-1)
			if self.active == False:
				self.victory_hide()
				Animation(z = -5.0, t='out_cubic').start(self.ids.game.board.pos1)
				print "TADY!"
			if self.ids.animations.active == True:
				Animation(angle = 390, t='out_cubic').start(self.ids.game.board.rot1)
				animation = Animation(angle = 390, t='out_cubic')
				animation.start(self.ids.game.cube.rot2)
				animation.bind(on_complete = self.next_action_finished)
			else:
				self.next_action_finished()
			self.moves_no = 0

	def next_action(self):
		if self.level_no < self.levels_unlocked:
			if self.moves_no > 0 and self.move_locked == False:
				self.write_stats(False)
			self.load_level(self.level_no+1)
			if self.active == False:
				self.victory_hide()
				print "TADY!!"
				Animation(z = -5.0, t='out_cubic').start(self.ids.game.board.pos1)
			if self.ids.animations.active == True:
				Animation(angle = 390, t='out_cubic').start(self.ids.game.board.rot1)
				animation = Animation(angle = 390, t='out_cubic')
				animation.start(self.ids.game.cube.rot2)
				animation.bind(on_complete = self.next_action_finished)
			else:
				self.next_action_finished()
			self.moves_no = 0
		
	def next_action_finished(self, *args):
		self.ids.game.cube.rotx.angle = 0
		self.ids.game.cube.roty.angle = 0
		self.ids.game.cube.rotz.angle = 0
		self.active = True
		self.move_locked = False

	def game_show(self, level_no):
		self.load_level(level_no)
		self.ids.game.reset_canvas()
		self.ids.game.cube.pos2.x = 0
		self.ids.game.cube.pos2.z = 0
		self.menu_rotate = False
		if self.ids.animations.active == True:
			Animation(x = -2, t='out_cubic').start(self.ids.game.cube.pos2)
			Animation(z = -2, t='out_cubic').start(self.ids.game.cube.pos2)
			Animation(y = self.height*0.497, t='out_cubic').start(self.ids.game_buttons)
			Animation(y = self.height*0.623, t='out_cubic').start(self.ids.game_labels)
			Animation(z = -5.0, t='out_cubic').start(self.ids.game.board.pos1)
			Animation(y = 0, t='out_cubic').start(self.ids.game.cube.pos1)
			Animation(z = -5.0, t='out_cubic').start(self.ids.game.cube.pos1)
			
			Animation(angle = 0, t='out_cubic').start(self.ids.game.cube.rotx)
			Animation(angle = 0, t='out_cubic').start(self.ids.game.cube.roty)
			
			anim = Animation(angle = 0, t='out_cubic')
			anim.start(self.ids.game.cube.rotz)
			anim.bind(on_complete=self.testdef)
		else:
			
			self.ids.game.cube.pos2.x = -2
			self.ids.game.cube.pos2.z = -2
			self.ids.game_buttons.y = self.height*0.497
			self.ids.game_labels.y = self.height*0.623
			self.ids.game.board.pos1.z = -5.0
			self.ids.game.cube.pos1.y = 0
			self.ids.game.cube.pos1.z = -5.0
			
			self.ids.game.cube.rotx.angle = 0
			self.ids.game.cube.roty.angle = 0
			self.ids.game.cube.rotz.angle = 0
			self.testdef()

	def testdef(self, *args):
		self.move_locked = False

	def game_hide(self):
		self.active = False
		self.move_locked = True
		self.menu_rotate = True
		if self.ids.animations.active == True:
			Animation(y = self.height, t='out_cubic').start(self.ids.game_buttons)
			Animation(y = self.height, t='out_cubic').start(self.ids.game_labels)
			Animation(x = 0, t='out_cubic').start(self.ids.game.cube.pos2)
			Animation(z = 0, t='out_cubic').start(self.ids.game.cube.pos2)
		else:
			self.ids.game_buttons.y = self.height
			self.ids.game_labels.y = self.height
			self.ids.game.cube.pos2.x = 0
			self.ids.game.cube.pos2.z = 0
		self.cube_get_far(90, -7, -1.3)
		
	def cube_get_far(self, angle, z ,y):
		if self.ids.animations.active == True:
			Animation(z = z, t='in_cubic').start(self.ids.game.cube.pos1)
			Animation(y = y, t='in_cubic').start(self.ids.game.cube.pos1)
			Animation(angle = angle, t='out_cubic').start(self.ids.game.cube.rotx)
			Animation(angle = angle, t='out_cubic').start(self.ids.game.cube.roty)
			Animation(angle = angle, t='out_cubic').start(self.ids.game.cube.rotz)
		else:
			self.ids.game.cube.pos1.z = z
			self.ids.game.cube.pos1.y = y
			self.ids.game.cube.rotx.angle = angle
			self.ids.game.cube.roty.angle = angle
			self.ids.game.cube.rotz.angle = angle
	
	def menu_show(self):
		if self.ids.animations.active == True:
			Animation(x = self.width*0.05, t='out_cubic').start(self.ids.menu_buttons_left)
			Animation(x = self.width*0.7, t='out_cubic').start(self.ids.menu_buttons_right)
			Animation(y = self.height*0.65, t='out_cubic').start(self.ids.corlit_label)
			Animation(z = 2, t='out_cubic').start(self.ids.game.board.pos1)
		else:
			self.ids.menu_buttons_left.x = self.width*0.05
			self.ids.menu_buttons_right.x = self.width*0.7
			self.ids.corlit_label.y = self.height*0.65
			self.ids.game.board.pos1.z = 2
		self.menu_rotate_user = True

	def menu_hide(self):
		if self.ids.animations.active == True:
			Animation(x = -self.width*0.25, t='out_cubic').start(self.ids.menu_buttons_left)
			Animation(x = self.width, t='out_cubic').start(self.ids.menu_buttons_right)
			Animation(y = self.height, t='out_cubic').start(self.ids.corlit_label)
		else:
			self.ids.menu_buttons_left.x = -self.width*0.25
			self.ids.menu_buttons_right.x = self.width
			self.ids.corlit_label.y = self.height
		self.menu_rotate_user = False

	def levels_show(self):
		self.ids.stars_labels.clear_widgets()
		for i in xrange(60):
			if i >= self.levels_unlocked:
				self.ids.stars_labels.add_widget(Label(text="[color=#AAAAAA]" + self.stars[i] + "[/color]", markup = True))
			else:
				self.ids.stars_labels.add_widget(Label(text=self.stars[i]))
			
		if self.ids.animations.active == True:
			Animation(y = self.height*0.1, t='out_cubic').start(self.ids.level_buttons)
			Animation(y = self.height*0.05, t='out_cubic').start(self.ids.stars_labels)
			Animation(x = self.width*0.9, t='out_cubic').start(self.ids.info_labels)
			Animation(y = self.height*0.775, t='out_cubic').start(self.ids.menu_button)
			Animation(y = self.height*0.1, t='out_cubic').start(self.ids.stars_info)
		else:
			self.ids.level_buttons.y = self.height*0.1
			self.ids.stars_labels.y = self.height*0.05
			self.ids.info_labels.x = self.width*0.9
			self.ids.menu_button.y = self.height*0.775
			self.ids.stars_info.y = self.height*0.1
		self.ids.game.board.pos1.z = 2
		self.cube_get_far(45, -42, 19)

	def levels_hide(self):
		if self.ids.animations.active == True:
			Animation(y = -self.height*0.8, t='out_cubic').start(self.ids.level_buttons)
			Animation(y = -self.height*0.85, t='out_cubic').start(self.ids.stars_labels)
			Animation(x = self.width*1.8, t='out_cubic').start(self.ids.info_labels)
			Animation(y = self.height, t='out_cubic').start(self.ids.menu_button)
			Animation(y = self.height, t='out_cubic').start(self.ids.stars_info)
			self.cube_get_far(90, -7, -1.3)
		else:
			self.ids.level_buttons.y = -self.height*0.8
			self.ids.stars_labels.y = -self.height*0.85
			self.ids.info_labels.x = self.width*1.8
			self.ids.menu_button.y = self.height
			self.ids.stars_info.y = self.height
			self.cube_get_far(90, -7, -1.3)

	def settings_show(self):
		if self.ids.animations.active == True:
			Animation(y = 0.15*self.height, t='out_cubic').start(self.ids.settings_label)
			Animation(y = 0.15*self.height, t='out_cubic').start(self.ids.settings_label_checkboxes)
			Animation(y = self.height*0.775, t='out_cubic').start(self.ids.menu_button)
		else:
			self.ids.settings_label.y = 0.15*self.height
			self.ids.settings_label_checkboxes.y = 0.15*self.height
			self.ids.menu_button.y = self.height*0.775
		self.cube_get_far(45, -42, 19)

	def settings_hide(self):
		if self.ids.animations.active == True:
			Animation(y = -0.7*self.height, t='out_cubic').start(self.ids.settings_label)
			Animation(y = -0.7*self.height, t='out_cubic').start(self.ids.settings_label_checkboxes)
			Animation(y = self.height, t='out_cubic').start(self.ids.menu_button)
		else:
			self.ids.settings_label.y = -0.7*self.height
			self.ids.settings_label_checkboxes.y = -0.7*self.height
			self.ids.menu_buttony = self.height
		self.cube_get_far(90, -7, -1.3)

		self.store_settings.put("music", volume=self.ids.music.active)
		self.store_settings.put("sounds", volume=self.ids.sound_volume.active)
		if self.ids.resume_unsolved.active == True:
			self.store_settings.put("resume", action="unsolved")
		elif self.ids.resume_untried.active == True:
			self.store_settings.put("resume", action="untried")
		elif self.ids.resume_imperfect.active == True:
			self.store_settings.put("resume", action="imperfect")
		if self.ids.animations.active == True:
			self.store_settings.put("UI", animations="True")
		elif self.ids.animations.active == False:
			self.store_settings.put("UI", animations="False")
		self.read_settings()

	def stats_show(self):
		if self.ids.animations.active == True:
			Animation(y = self.height*0.775, t='out_cubic').start(self.ids.menu_button)
			Animation(y = self.height*0.775, t='out_cubic').start(self.ids.stats_prev)
			Animation(y = self.height*0.625, t='out_cubic').start(self.ids.stats_next)
			Animation(y = self.height*0.475, t='out_cubic').start(self.ids.stats_first)
			Animation(y = self.height*0.325, t='out_cubic').start(self.ids.stats_last)
			Animation(x = self.width*0.93, t='out_cubic').start(self.ids.stats_copy)
			Animation(y = self.height*0.1, t='out_cubic').start(self.ids.stats_label_scroll)
		else:
			self.ids.menu_button.y = self.height*0.775
			self.ids.stats_prev.y = self.height*0.775
			self.ids.stats_next.y = self.height*0.625
			self.ids.stats_first.y = self.height*0.475
			self.ids.stats_last.y = self.height*0.325
			self.ids.stats_copy.x = self.width*0.93
			self.ids.stats_label_scroll.y = self.height*0.1
		self.cube_get_far(45, -42, 19)
		self.stats_order(self.stats_from_level)

	def copy_stats(self, *args):
		level_stats = [i.strip().split() for i in open("stats.txt").readlines()]
		Clipboard.put(str(level_stats), "UTF8_STRING")

	def stats_order(self, from_level, *args):
		self.ids.stats_label.clear_widgets()
		stats = self.store_stats.get("level_" +  str(self.level_no))
		head_list = ["Tries", "Finished", "Total moves", "Finished moves",
					"Least moves", "Most moves", "Average moves",
					"Possible moves", "Total time", "Finished time",
					"Shortest time", "Longest time", "Average time"]
		head_list_raw = ["tries", "finished", "total_moves", "finished_moves",
					"least_moves", "most_moves", "average_moves",
					"possible_moves", "total_time", "finished_time",
					"shortest_time", "longest_time", "average_time"]
		
		for i in xrange(5):
			self.ids.stats_label.add_widget(Button(text="Level " + str(i+from_level) + "\n" + self.stars[i+from_level-1], height=0.145*Window.height, size_hint=(1./14, None), halign="center"))

		for i in xrange(0,7):
			for j in xrange(5):
				self.ids.stats_label.add_widget(Button(text=head_list[i] + "\n" + str(self.store_stats.get("level_" + str(j+from_level))[head_list_raw[i]]), height=0.08*Window.height, size_hint=(1, None), halign="center"))

		for i in xrange(5):
			self.ids.stats_label.add_widget(Button(text="Possible moves\n" + str(self.possible_moves[i-1+from_level]), height=0.08*Window.height, size_hint=(1, None), halign="center"))

		for i in xrange(8,13):
			for j in xrange(5):
				value = self.store_stats.get("level_" + str(j+from_level))[head_list_raw[i]]
				if value%3600/60 < 10 and value%60 < 10:
					value = str(value/3600) + ":0" + str(value%3600/60) + ":0" + str(value%60)
				elif value%3600/60 < 10:
					value = str(value/3600) + ":0" + str(value%3600/60) + ":" + str(value%60)
				elif value%60 < 10:
					value = str(value/3600) + ":" + str(value%3600/60) + ":0" + str(value%60)
				else:
					value = str(value/3600) + ":" + str(value%3600/60) + ":" + str(value/60)
				self.ids.stats_label.add_widget(Button(text=head_list[i] + "\n" + value, height=0.08*Window.height, size_hint=(1, None), halign="center"))

	def stats_hide(self):
		if self.ids.animations.active == True:
			Animation(y = self.height, t='out_cubic').start(self.ids.menu_button)
			Animation(y = self.height*1.45, t='out_cubic').start(self.ids.stats_prev)
			Animation(y = self.height*1.30, t='out_cubic').start(self.ids.stats_next)
			Animation(y = self.height*1.15, t='out_cubic').start(self.ids.stats_first)
			Animation(y = self.height, t='out_cubic').start(self.ids.stats_last)
			Animation(x = self.width*1.00, t='out_cubic').start(self.ids.stats_copy)
			animation = Animation(y = -self.height*0.84, t='out_cubic')
			animation.start(self.ids.stats_label_scroll)
			animation.bind(on_complete=self.cleaner)
		else:
			self.ids.menu_button.y = self.height
			self.ids.stats_prev.y = self.height*1.45
			self.ids.stats_next.y = self.height*1.30
			self.ids.stats_first.y = self.height*1.15
			self.ids.stats_last.y = self.height
			self.ids.stats_copy.x = self.width*1.00
			self.ids.stats_label_scroll.y = -self.height*0.84
			self.cleaner()
		self.cube_get_far(90, -7, -1.3)

	def cleaner(self, *args):
		self.ids.stats_label.clear_widgets()

	def credits_show(self):
		if self.ids.animations.active == True:
			Animation(y = 0.1*self.height, t='out_cubic').start(self.ids.credits_label)
			Animation(y = self.height*0.775, t='out_cubic').start(self.ids.menu_button)
		else:
			self.ids.credits_label.y = 0.1*self.height
			self.ids.menu_button.y = self.height*0.775
		self.cube_get_far(45, -42, 19)

	def credits_hide(self):
		if self.ids.animations.active == True:
			Animation(y = -0.8*self.height, t='out_cubic').start(self.ids.credits_label)
			Animation(y = self.height, t='out_cubic').start(self.ids.menu_button)
		else:
			self.ids.credits_label.y = -0.8*self.height
			self.ids.menu_button.y = self.height




	def count_stars(self, *args):
		self.total_stars = 0
		self.levels_unlocked = 0
		self.unlock2_label = ""
		self.unlock3_label = ""
		self.unlock4_label = ""
		self.unlock5_label = ""
		self.unlock6_label = ""
		for i in xrange(6):
			for j in xrange(10):
				if self.stars[i*10+j] == "*":
					self.total_stars += 1
				elif self.stars[i*10+j] == "**":
					self.total_stars += 2
				elif self.stars[i*10+j] == "***":
					self.total_stars += 3
			if self.total_stars >= 20*i:
				self.levels_unlocked += 10
		if 20 - self.total_stars > 0:
			self.unlock2_label = str(20-self.total_stars) + "x *\nleft"
		if 40 - self.total_stars > 0:
			self.unlock3_label = str(40-self.total_stars) + "x *\nleft"
		if 60 - self.total_stars > 0:
			self.unlock4_label = str(60-self.total_stars) + "x *\nleft"
		if 80 - self.total_stars > 0:
			self.unlock5_label = str(80-self.total_stars) + "x *\nleft"
		if 100 - self.total_stars > 0:
			self.unlock6_label = str(100-self.total_stars) + "x *\nleft"

		self.ids.level_buttons.clear_widgets()
		for i in range(self.levels_unlocked):
			self.ids.level_buttons.add_widget(Button(text=str(i + 1), on_release=eval("partial(self.resume_action, " + str(i + 1) + ")")))
		for i in range(60-self.levels_unlocked):
			self.ids.level_buttons.add_widget(Button(text="[color=#AAAAAA]" + str(i + 1 + self.levels_unlocked) + "[/color]", markup = True))

	def play_sound(self):
		if self.sound.status != "stop":
			self.sound.stop()
		if self.ids.sound_volume.active == True:
			self.sound.play()

	def on_touch_down(self, touch):
		self.touch_down = list(touch.pos)
		self.touch_ready = True
		if self.menu_rotate_user == True:
			self.temp_touch_time = self.time2 # for rotating cube in menu
		super(Root, self).on_touch_down(touch) 

	def on_touch_up(self, touch):
		if self.move_locked == False and self.touch_ready == True and self.rolling == False:
			self.active = True
			if list(touch.pos)[0] > self.touch_down[0] + Window.width*0.03 and math.fabs(list(touch.pos)[1] - self.touch_down[1]) < (list(touch.pos)[0] - self.touch_down[0])/2 and self.ids.game.cube.pos2.x != 2:
				self.roll("right")
			elif list(touch.pos)[0] < self.touch_down[0] - Window.width*0.03 and math.fabs(list(touch.pos)[1] - self.touch_down[1]) < (self.touch_down[0] - list(touch.pos)[0])/2 and self.ids.game.cube.pos2.x != -2:
				self.roll("left")
			elif list(touch.pos)[1] > self.touch_down[1] + Window.height*0.03 and math.fabs(list(touch.pos)[0] - self.touch_down[0]) < (list(touch.pos)[1] - self.touch_down[1])/2 and self.ids.game.cube.pos2.z != -2:
				self.roll("up")
			elif list(touch.pos)[1] < self.touch_down[1] - Window.height*0.03 and math.fabs(list(touch.pos)[0] - self.touch_down[0]) < (self.touch_down[1] - list(touch.pos)[1])/2 and self.ids.game.cube.pos2.z != 0:
				self.roll("down")
			self.touch_ready = False
		if math.fabs(list(touch.pos)[1] - self.touch_down[1]) < Window.height*0.01 and math.fabs(list(touch.pos)[0] - self.touch_down[0]) < Window.width*0.01 and self.won == True and list(touch.pos)[0] > 0.07*Window.width:
			self.next_action()
			print "ZEBY", list(touch.pos)[0], 0.07*Window.width
		if self.menu_rotate_user == True:
			y_axe = self.touch_down[0] - list(touch.pos)[0] # horizontal
			x_axe = self.touch_down[1] - list(touch.pos)[1] # vertical
			time = self.time2 - self.temp_touch_time
			self.rotation_speed_y += y_axe / time / 1000
			self.rotation_speed_x += x_axe / time / 1000
		super(Root, self).on_touch_up(touch)
		print list(touch.pos)[0]

	def roll(self, direction):
		if direction == "right":
			self.ids.game.cube.pos2.x += 1
			self.ids.game.cube.pos2.y -= 1
			self.ids.game.cube.pos3.x = -1
			self.ids.game.cube.pos3.y = 1
			animation = Animation(angle = -90, t='in_out_quad', duration = 0.5)
			animation.start(self.ids.game.cube.rotz)
		elif direction == "left":       
			self.ids.game.cube.pos2.x -= 1
			self.ids.game.cube.pos2.y -= 1
			self.ids.game.cube.pos3.x = 1
			self.ids.game.cube.pos3.y = 1
			animation = Animation(angle = 90, t='in_out_quad', duration = 0.5)
			animation.start(self.ids.game.cube.rotz)
		elif direction == "up":
			self.ids.game.cube.pos2.z -= 1
			self.ids.game.cube.pos2.y -= 1
			self.ids.game.cube.pos3.z = 1
			self.ids.game.cube.pos3.y = 1
			animation = Animation(angle = -90, t='in_out_quad', duration = 0.5)
			animation.start(self.ids.game.cube.rotx)
		elif direction == "down":
			self.ids.game.cube.pos2.z += 1
			self.ids.game.cube.pos2.y -= 1
			self.ids.game.cube.pos3.z = -1
			self.ids.game.cube.pos3.y = 1
			animation = Animation(angle = 90, t='in_out_quad', duration = 0.5)
			animation.start(self.ids.game.cube.rotx)
		animation.bind(on_complete = partial(self.roll_reset, direction))
		self.rolling = True

	def roll_reset(self, direction, *args):
		if direction == "right":
			self.ids.game.cube.rotz.angle = 0
			self.ids.game.cube.pos2.x += 1
			self.ids.game.cube.pos2.y += 1
			self.ids.game.cube.colors["right"] = self.ids.game.cube.colors["top"]
			self.ids.game.cube.colors["top"] = self.ids.game.cube.colors["left"]
			self.ids.game.cube.colors["left"] = self.ids.game.cube.colors["bottom"]

			temp_goal_color = self.ids.game.cube.goal_colors["right"]
			self.ids.game.cube.goal_colors["right"] = self.ids.game.cube.goal_colors["top"]
			self.ids.game.cube.goal_colors["top"] = self.ids.game.cube.goal_colors["left"]
			self.ids.game.cube.goal_colors["left"] = self.ids.game.cube.goal_colors["bottom"]
			self.ids.game.cube.goal_colors["bottom"] = temp_goal_color

		elif direction == "left":
			self.ids.game.cube.rotz.angle = 0
			self.ids.game.cube.pos2.x -= 1
			self.ids.game.cube.pos2.y += 1
			self.ids.game.cube.colors["left"] = self.ids.game.cube.colors["top"]
			self.ids.game.cube.colors["top"] = self.ids.game.cube.colors["right"]
			self.ids.game.cube.colors["right"] = self.ids.game.cube.colors["bottom"]

			temp_goal_color = self.ids.game.cube.goal_colors["left"]
			self.ids.game.cube.goal_colors["left"] = self.ids.game.cube.goal_colors["top"]
			self.ids.game.cube.goal_colors["top"] = self.ids.game.cube.goal_colors["right"]
			self.ids.game.cube.goal_colors["right"] = self.ids.game.cube.goal_colors["bottom"]
			self.ids.game.cube.goal_colors["bottom"] = temp_goal_color

		elif direction == "up":
			self.ids.game.cube.rotx.angle = 0
			self.ids.game.cube.pos2.z -= 1
			self.ids.game.cube.pos2.y += 1
			self.ids.game.cube.colors["back"] = self.ids.game.cube.colors["top"]
			self.ids.game.cube.colors["top"] = self.ids.game.cube.colors["front"]
			self.ids.game.cube.colors["front"] = self.ids.game.cube.colors["bottom"]

			temp_goal_color = self.ids.game.cube.goal_colors["back"]
			self.ids.game.cube.goal_colors["back"] = self.ids.game.cube.goal_colors["top"]
			self.ids.game.cube.goal_colors["top"] = self.ids.game.cube.goal_colors["front"]
			self.ids.game.cube.goal_colors["front"] = self.ids.game.cube.goal_colors["bottom"]
			self.ids.game.cube.goal_colors["bottom"] = temp_goal_color

		elif direction == "down":
			self.ids.game.cube.rotx.angle = 0
			self.ids.game.cube.pos2.z += 1
			self.ids.game.cube.pos2.y += 1
			self.ids.game.cube.colors["front"] = self.ids.game.cube.colors["top"]
			self.ids.game.cube.colors["top"] = self.ids.game.cube.colors["back"]
			self.ids.game.cube.colors["back"] = self.ids.game.cube.colors["bottom"]

			temp_goal_color = self.ids.game.cube.goal_colors["front"]
			self.ids.game.cube.goal_colors["front"] = self.ids.game.cube.goal_colors["top"]
			self.ids.game.cube.goal_colors["top"] = self.ids.game.cube.goal_colors["back"]
			self.ids.game.cube.goal_colors["back"] = self.ids.game.cube.goal_colors["bottom"]
			self.ids.game.cube.goal_colors["bottom"] = temp_goal_color

		self.play_sound()
		self.ids.game.cube.colors["bottom"] = self.ids.game.board.colors[int(self.ids.game.cube.pos2.x/2 + 1 - self.ids.game.cube.pos2.z/2*3)]
		self.ids.game.cube.pos3.x = 0
		self.ids.game.cube.pos3.y = 0
		self.ids.game.cube.pos3.z = 0
		self.ids.game.cube.rotx.angle = 0
		self.ids.game.cube.rotz.angle = 0
		self.moves_no += 1
		if self.moves_no == 1:
			self.moves_no_title = "\nmove"
		else:
			self.moves_no_title = "\nmoves"
		self.rolling = False
		if self.active == True:
			self.ids.game.reset_canvas()
		self.victory_checker()

	def load_level(self, level_no, *args):
		self.won = False
		
		for i in xrange(60):
			stats = self.store_stats.get("level_" + str(i+1))
			if stats["finished"] == 0:
				self.stars[i] = ""
			elif stats["least_moves"] == self.possible_moves[i]:
				self.stars[i] = "***"
			elif stats["least_moves"] < self.possible_moves[i]:
				self.stars[i] = "!"
			elif stats["least_moves"] < 2*self.possible_moves[i]:
				self.stars[i] = "**"
			else:
				self.stars[i] = "*"

		self.ids.game.board.colors = self.store_levels.get("level_" + str(level_no))["board_colors"]
		self.level_no = level_no
		if self.level_no == 1 or self.level_no == 21 or self.level_no == 31 or self.level_no == 41 or self.level_no == 51:
			self.level_no_ending = "st"
		elif self.level_no == 2 or self.level_no == 22 or self.level_no == 32 or self.level_no == 42 or self.level_no == 52:
			self.level_no_ending = "nd"
		elif self.level_no == 3 or self.level_no == 23 or self.level_no == 33 or self.level_no == 43 or self.level_no == 53:
			self.level_no_ending = "rd"
		else:
			self.level_no_ending = "th"
		self.time = 0
		self.moves_no = 0
		self.active = False
		self.ids.game.cube.view_mode = "Goal"
		self.ids.game.cube.pos2.x = -2
		self.ids.game.cube.pos2.z = -2
		self.moves_best = self.store_stats.get("level_" + str(level_no))["least_moves"]
		self.moves_possible = self.possible_moves[level_no-1]

		try:
			for i in xrange(self.ids.game.board.depth): 
				for j in xrange(3):
					r = float(int(self.ids.game.board.colors[i*3 + j][0:2], 16))/255
					g = float(int(self.ids.game.board.colors[i*3 + j][2:4], 16))/255
					b = float(int(self.ids.game.board.colors[i*3 + j][4:6], 16))/255
					self.ids.game.board.colors[i*3 + j] = (r, g, b)
		except:
			pass

		self.ids.game.cube.colors = {}
		goal_colors = self.store_levels.get("level_" + str(level_no))["goal_colors"]
		self.ids.game.cube.goal_colors = {}


		try:
			for i in self.ids.game.cube.sides:
				if i == "bottom":
					self.ids.game.cube.colors[i] = self.ids.game.board.colors[3]
				else:
					self.ids.game.cube.colors[i] = (.93, .93, .93)
				
				r = float(int(goal_colors[int(self.ids.game.cube.sides[i])][0:2], 16))/255
				g = float(int(goal_colors[int(self.ids.game.cube.sides[i])][2:4], 16))/255
				b = float(int(goal_colors[int(self.ids.game.cube.sides[i])][4:6], 16))/255
				goal_colors[int(self.ids.game.cube.sides[i])] = (r, g, b)
				self.ids.game.cube.goal_colors[i] = goal_colors[int(self.ids.game.cube.sides[i])]
		except:
			for i in self.ids.game.cube.sides:
				if i == "bottom":
					self.ids.game.cube.colors[i] = self.ids.game.board.colors[3]
				else:
					self.ids.game.cube.colors[i] = (.93, .93, .93)
				self.ids.game.cube.goal_colors[i] = goal_colors[int(self.ids.game.cube.sides[i])]
		self.ids.game.reset_canvas()

	def victory_checker(self):
		temp_victory_checker = 0
		for i in self.ids.game.cube.sides:
			if self.ids.game.cube.colors[i] == self.ids.game.cube.goal_colors[i] or self.ids.game.cube.goal_colors[i] == (.9333333333333333, .9333333333333333, .9333333333333333): temp_victory_checker += 1
		if temp_victory_checker == 6:
			self.move_locked = True
			self.ids.game.reset_canvas()
			self.write_stats(True)
			if self.moves_no < self.moves_best or self.moves_best == 0:
				self.moves_best = self.moves_no
			self.count_stars()
			self.victory_show()
			self.won = True

	def write_stats(self, finished):
		stats = self.store_stats.get("level_" + str(self.level_no))
		if finished == True:
			
			if self.moves_no < stats["least_moves"] or stats["least_moves"] == 0:
				least_moves = self.moves_no
			else:
				least_moves = stats["least_moves"]

			if self.moves_no > stats["most_moves"]:
				most_moves = self.moves_no
			else:
				most_moves = stats["most_moves"]

			if self.time < stats["shortest_time"] or stats["shortest_time"] == 0:
				shortest_time = int(self.time)
			else:
				shortest_time = stats["shortest_time"]

			if self.time > stats["longest_time"]:
				longest_time = int(self.time)
			else:
				longest_time = stats["longest_time"]

			self.store_stats.put("level_" + str(self.level_no),
								tries=stats["tries"]+1,
								finished=stats["finished"]+1,
								total_moves=stats["total_moves"]+self.moves_no,
								finished_moves=stats["finished_moves"]+self.moves_no,
								least_moves=least_moves,
								most_moves=most_moves,
								average_moves=(stats["finished_moves"]+self.moves_no)/(stats["finished"]+1),
								total_time=stats["total_time"]+int(self.time),
								finished_time=stats["finished_time"]+int(self.time),
								shortest_time=shortest_time,
								longest_time=longest_time,
								average_time=(stats["finished_time"]+int(self.time))/(stats["finished"]+1))
		else:
			self.store_stats.put("level_" + str(self.level_no),
							tries=stats["tries"]+1,
							finished=stats["finished"],
							total_moves=stats["total_moves"]+self.moves_no,
							finished_moves=stats["finished_moves"],
							least_moves=stats["least_moves"],
							most_moves=stats["most_moves"],
							average_moves=stats["average_moves"],
							total_time=stats["total_time"]+int(self.time),
							finished_time=stats["finished_time"],
							shortest_time=stats["shortest_time"],
							longest_time=stats["longest_time"],
							average_time=stats["average_time"])

		stats = self.store_stats.get("level_" + str(self.level_no))
		if stats["least_moves"] == self.possible_moves[self.level_no-1]:
			self.stars[self.level_no-1] = "***"
		elif stats["least_moves"] < self.possible_moves[self.level_no-1]:
			self.stars[self.level_no-1] = "!"
		elif stats["least_moves"] < 2*self.possible_moves[self.level_no-1]:
			self.stars[self.level_no-1] = "**"
		else:
			self.stars[self.level_no-1] = "*"

	def victory_show(self):
		self.menu_rotate = True
		self.active = False
		if self.ids.animations.active == True:
			Animation(victory_y = Window.height*0.3).start(self)
			Animation(z = 2, t='out_cubic').start(self.ids.game.board.pos1)	
			Animation(z = -42, t='in_cubic').start(self.ids.game.cube.pos1)
			Animation(y = 19, t='in_cubic').start(self.ids.game.cube.pos1)
			Animation(x = 0, t='in_cubic').start(self.ids.game.cube.pos2)
			Animation(z = 0, t='in_cubic').start(self.ids.game.cube.pos2)
			Animation(angle = 180, t='out_cubic').start(self.ids.game.cube.rotx)
			Animation(angle = 180, t='out_cubic').start(self.ids.game.cube.roty)
			Animation(angle = 180, t='out_cubic').start(self.ids.game.cube.rotz)
		else:
			self.victory_y = Window.height*0.3
			self.ids.game.board.pos1.z = 2
			self.ids.game.cube.pos1.z = -42
			self.ids.game.cube.pos1.y = 19
			self.ids.game.cube.pos2.x = 0
			self.ids.game.cube.pos2.z = 0
			self.ids.game.cube.rotx.angle = 180
			self.ids.game.cube.roty.angle = 180
			self.ids.game.cube.rotz.angle = 180

	def victory_hide(self):
		self.menu_rotate = False
		self.won = False
		if self.ids.animations.active == True:
			Animation(victory_y = -Window.height*0.6).start(self)		
			Animation(z = -5.0, t='out_cubic', duration=0.2).start(self.ids.game.cube.pos1)
			Animation(y = 0, t='in_cubic', duration=0.2).start(self.ids.game.cube.pos1)
			Animation(angle = 0, t='out_cubic').start(self.ids.game.cube.rotx)
			Animation(angle = 0, t='out_cubic').start(self.ids.game.cube.roty)
			Animation(angle = 0, t='out_cubic').start(self.ids.game.cube.rotz)
		else:
			self.victory_y = -Window.height*0.6
			self.ids.game.board.pos1.z = -5.0		
			self.ids.game.cube.pos1.z = -5.0
			self.ids.game.cube.pos1.y = 0
			self.ids.game.cube.rotx.angle = 0
			self.ids.game.cube.rotyangle = 0
			self.ids.game.cube.rotz.angle = 0

class CorlitApp(App): 
	def build(self):
		root = Root()
		Clock.schedule_interval(root.update, 1.0 / 60.0)
		return root

	def on_pause(self):
		root.soundtrack.stop()
		return True

	def on_resume(self):
		root.soundtrack.play()
		return True

if __name__ == '__main__':
	CorlitApp().run()